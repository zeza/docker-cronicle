FROM node:7.4.0-slim

RUN curl -s https://raw.githubusercontent.com/jhuckaby/Cronicle/master/bin/install.js | node

COPY docker-entrypoint.sh /docker-entrypoint.sh
COPY conf/config.json /opt/cronicle/conf/

RUN chown -R node.node /opt/cronicle

USER node

ENV HOME /home/node
ENV IS_MASTER true

EXPOSE 3012
EXPOSE 3014
ENTRYPOINT ["/docker-entrypoint.sh"]
