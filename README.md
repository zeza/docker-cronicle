# Build 
docker build -f Dockerfile -t zippa/cronicle:latest .

# Delete 
docker rm -f cronicle

# Run Master
docker run -d --name=cronicle -p 3012:3012 -p 3014:3014 zippa/cronicle:latest

# Run Slave
#docker run -d --name=cronicle_slave IS_MASTER=false zippa/cronicle:latest

# Exec
docker exec -ti cronicle bash
